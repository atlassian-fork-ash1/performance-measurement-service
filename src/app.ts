import * as Logger from 'bunyan';
import Metrics from '@atlassian/metrics';
import * as Koa from 'koa';
import * as Router from 'koa-router';
import config from './config';
import middleware from './middleware';
import { basic, measure } from './routing';

export default function createApp(logger: Logger) {
  const app = new Koa();

  // initialise metrics collector and logger
  const metrics = new Metrics(process.env.MICROS_SERVICE || 'performance-measurement-service');
  const metricLogger = logger.child({ subsystem: 'metric' });
  metrics.on('metric', (name: string, componentPath: any, data: any) => {
    if (process.env.NODE_ENV !== 'production') {
      metricLogger.info({ type: name, componentPath, value: data.value }, '%s event on %s', name, componentPath);
    }
  });

  // Sending memory usage of this worker periodictly.
  setInterval(() => {
    metrics.timing('app.memory', process.memoryUsage().rss, undefined, [`pid:${process.pid}`, `worker`]);
  }, 60000);

  // middleware
  app.use(middleware({ cors: config.cors, logger, metrics }));

  // routing
  app.use(basic(new Router()));
  app.use(measure(new Router()));

  return app;
}
