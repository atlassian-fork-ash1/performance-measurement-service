import createLogger from '@atlassian/logger';

import getApp from './app';
import config from './config';
import * as pkg from '../package.json';

const logger = createLogger({ level: config.logger.defaultLevel, name: pkg.name });
const app = getApp(logger);

Error.stackTraceLimit = Infinity;

app.on('error', (err) => {
  /**
   * This would capture errors that falls out of the middleware chain.
   * For example, event errors or stream errors
   */
  if (err.code && err.code.startsWith('HPE')) {
    logger.warn({ err });
  } else {
    logger.error({ err });
  }
});

app.listen(config.port);

logger.info(`Running service on: http://localhost:${config.port}`);

// Make sure uncaught exceptions are handled and logged
const processLogger = logger.child({ subsystem: 'process' });
process.on('unhandledRejection', (reason: Error, p: Promise<any>) => {
  processLogger.warn({ err: reason, promise: p }, 'Unhandled Rejection');
});

process.on('uncaughtException', (err: Error) => {
  processLogger.fatal({ err, type: 'uncaughtException' }, err);
});

process.on('exit', (code: any) => {
  processLogger.info(`About to exit with code: ${code}`);
});
