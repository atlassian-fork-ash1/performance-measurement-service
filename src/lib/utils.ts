import { IMiddleware } from 'koa-router';
import * as puppeteer from 'puppeteer';
import { Context } from './context';

export interface Payload {
  url: string;
  tests: { [key: string]: string };
}

export interface PerformanceMetrics {
  metrics: Array<{ name: string; value: number }>;
}

export const withEndpoint = (controller: IMiddleware, endpoint: string): IMiddleware => {
  return async (ctx: Context, next: () => Promise<any>) => {
    ctx.state[Symbol.for('endpoint')] = endpoint;
    await controller(ctx, next);
  };
};

export const isValidPayload = (body: Payload) => {
  if (!body || !body.url || !body.tests) {
    return false;
  }

  return true;
};

export const initializeBrowser = async () => {
  const browser = await puppeteer.launch({
    executablePath: process.env.CHROME_BIN || undefined,
    args: ['--no-sandbox', '--headless', '--disable-gpu', '--disk-cache-size=0', '--disable-setuid-sandbox']
  });

  return browser;
};

export const getTime = async (page: puppeteer.Page) => await page.evaluate(() => window.performance.now());

export const getPerformanceMetric = (name: string, performanceMetrics: PerformanceMetrics): number | undefined => {
  const { metrics } = performanceMetrics;
  for (let i = 0, count = metrics.length; i < count; i++) {
    if (metrics[i].name === name) {
      return metrics[i].value;
    }
  }
};

export const capitalizeFirstLetter = (str: string) => str.charAt(0).toUpperCase() + str.slice(1);
