import { Browser, Page } from 'puppeteer';
import Command, { Options } from './Command';

export default class Goto implements Command {
  groupId: string;
  packageId: string;
  exampleId: string;

  constructor(groupId?: string, packageId?: string, exampleId?: string) {
    if (!groupId || !packageId || !exampleId) {
      throw new Error('Required arguments are missing for `goto`');
    }
    this.groupId = groupId;
    this.packageId = packageId;
    this.exampleId = exampleId;
  }

  async execute(page: Page, browser: Browser, options: Options) {
    await page.goto(
      `${options.baseUrl}/examples.html?groupId=${this.groupId}&packageId=${this.packageId}&exampleId=${this.exampleId}`
    );
    await page.waitForSelector('#examples');

    return {};
  }
}
