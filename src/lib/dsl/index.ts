import { Parjs, ReplyKind } from 'parjs';
import { Browser, Page } from 'puppeteer';
import { Options } from './commands/Command';
import makeCommand from './make-command';

const input = `
goto editor editor-core full-page
quick_insert table
click cell 1 1
type "Hello hello"
insert_adf {"version":1,"type":"doc","content":[{"type":"paragraph","content":[{"type":"emoji","attrs":{"shortName":":joy:","id":"1f602","text":"😂"}}]}]}
`;

const pIdent = Parjs.letter.then(
  Parjs.letter
    .or(Parjs.digit)
    .or('-')
    .many()
).str;

const pStr = Parjs.string('"').q.then(Parjs.anyChar.or(Parjs.whitespaces).manyTill('"').str);

const pNumber = Parjs.float();

const pBool = Parjs.anyStringOf('true', 'false').map((v) => v === 'true');

// tslint:disable-next-line:variable-name
let _pJsonValue: any = null;
const pJsonValue = Parjs.late(() => _pJsonValue);

const pArray = pJsonValue.manySepBy(',').between('[', ']');

const pObjectProperty = pStr
  .then(Parjs.string(':').between(Parjs.whitespaces).q)
  .then(pJsonValue)
  .between(Parjs.whitespaces);

const pObject = pObjectProperty
  .manySepBy(',')
  .between('{', '}')
  .map((properties) =>
    properties.reduce((obj, [key, value]) => {
      (obj as any)[key] = value;
      return obj;
    }, {})
  );
_pJsonValue = Parjs.any(pStr, pNumber, pBool, pArray, pObject).between(Parjs.whitespaces);

const pArgs = Parjs.any(pIdent, pStr, pNumber, pObject, pArray).manySepBy(Parjs.space);

const pStep = Parjs.seq(
  Parjs.regexp(/^\w+/m).str.map((s) => s.toLowerCase()),
  Parjs.space.q.maybe(),
  pArgs,
  Parjs.newline.q.maybe()
).map(makeCommand);

const dslParser = pStep.between(Parjs.whitespaces).manyTill(Parjs.eof);

export const runDsl = async (browser: Browser, page: Page, options: Options, dsl: string) => {
  const output = dslParser.parse(dsl);
  if (output.kind === ReplyKind.Ok) {
    return output.value.reduce(
      (promise, command) =>
        command ? promise.then((context) => command.execute(page, browser, options, context)) : promise,
      Promise.resolve({})
    );
  } else {
    throw new Error(output.trace.reason);
  }
};

// console.log(dslParser.parse(input));
