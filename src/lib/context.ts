import { Tracer } from '@atlassian/koa-tracer';
import { Logger } from '@atlassian/logger';
import Metrics from '@atlassian/metrics';
import { IRouterContext } from 'koa-router';
import { ApplicationConfig } from '../config';

export interface Context extends IRouterContext {
  config: ApplicationConfig;
  logger: Logger;
  logRequest: boolean;
  metrics: Metrics;
  tracer: Tracer;
}
