import { Context } from '../lib/context';

export async function health(ctx: Context) {
  ctx.logRequest = false;
  ctx.status = 200;
}
